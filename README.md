## Bookly
`npm start`  to start the project
###
<img src="./public/project-1.jpg"
     alt="First page"
     style="float: left; margin-right: 10px;" height="162px" width="75px" /><img src="./public/project-2.jpg"
     alt="First page"
     style="float: left; margin-right: 10px;" height="162px" width="75px" /><img src="./public/project-3.jpg"
     alt="First page"
     style="float: left; margin-right: 10px;" height="162px" width="75px" /><img src="./public/project-4.jpg"
     alt="First page"
     style="float: left; margin-right: 10px;" height="162px" width="75px" /><img src="./public/project-5.jpg"
     alt="First page"
     style="float: left; margin-right: 10px;" height="162px" width="75px" /><img src="./public/project-6.jpg"
     alt="First page"
     style="float: left; margin-right: 10px;" height="162px" width="75px" /><img src="./public/project-7.jpg"
     alt="First page"
     style="float: left; margin-right: 10px;" height="162px" width="75px" /><img src="./public/project-8.jpg"
     alt="First page"
     style="float: left; margin-right: 10px;" height="162px" width="75px" /><img src="./public/project-9.jpg"
     alt="First page"
     style="float: left; margin-right: 10px;" height="162px" width="75px" />

 ###


```

`375 px to 812px` This app is static CSS design as featured in Figma design rulles of the project. When started the browser must be switched to `375 px to 812px`.

```

## Objective
The app have 7 screens. The personalization screen takes user input from choices and saves the choices in locale storage.

## Used technologies
- React, CSS, HTML, JS
- Different React modules
1. React router
2. Hooks- useEffect, useState, useParams

- [ x ] able to personalize your account preferences
- [ x ] preferences must be saved in localStorage
- [ x ] must be able to skip the preferences
- [ x ] must be greeted with the "Books" screen which contains all of the user's books
- [ x ] able to search the books
- [ x ] able to play the books
- [ x ] able to go back to the "Books" screen
- [ x ] must be greeted with the "On-Boarding" screen

###
## Thank you!
