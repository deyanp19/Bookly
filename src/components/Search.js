import styles from './Search.module.css';

function Search(props) {
    if (props.typeSearch==='placeholder') {
        return (
            <div className={styles.formBoxSearch}>
                <form className={styles.form}>
                    <input onChange={props.onSearch} className={styles.input} id="search" type='text' name="search" placeholder="Placeholder" />
                </form>
            </div>
    
           
        );
    } 

    return (
        <div className={styles.formBox}>
                <label className={styles.label} >My Books </label>
        <form className={styles.form}>
            <input onChange={props.onSearch} className={styles.input} id="search" type='text' name="search" placeholder="Search Books of Author ..." />
            </form>
        </div>

       
    );
}

export default Search;