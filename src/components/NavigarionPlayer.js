import { Link } from 'react-router-dom';
import styles from './NavigationPlayer.module.css';


function NavigationPlayer(props) {
    return (
        <div className={styles.navPlayer}>
            <Link to='/library'> <div className={styles.arrowLeft}>
               <img src='/Arrow-Left2.png' alt='arrow left' />
            </div></Link>
            <div className={styles.titleBox}>{props.title}</div>
            <ul className={styles.threeDots}>
           { ['one','two','three'].map((x,i)=>{
           return <li key={i.toString()} className={x} ></li>
           })}
           
        </ul> 
        </div>

    );
}

export default NavigationPlayer;